﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GridWarGameLogic
{
    public class GridBoard
    {
        public int Row;
        public int Col;
        public int[,] board;

        public GridBoard()
        {
            this.Row = 6;
            this.Col = 6;
            board = new int[this.Row, this.Col];
        }

       
        public void InsertValueIntoBoard(int row, int col, int value)
        {
            board[row, col] = value;
        }

        

        // Check row and column are valid
        public bool isValidPosition(int row, int col)
        {
            if ((row >= 0 && row <= Row - 1) && (col >= 0 && col <= Col - 1)) return true;
            return false;
        }

        // Check for available position 
        public bool isPositionAvailable(int row, int col)
        {
            if (isValidPosition(row, col) && board[row, col] == 0) return true;
            //Console.WriteLine("Position is not available");
            return false;
        }

    }
}
