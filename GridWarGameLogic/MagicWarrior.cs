﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GridWarGameLogic
{
    public class MagicWarrior : Warrior
    {

        public String MagicWarriorName = "MagicWarrior";
        public static int MagicWarriorNumer = 0;

        public Staff StaffForWarrior;

        public MagicWarrior() 
        {
            this.Name = MagicWarriorName + (++MagicWarriorNumer);
            this.Hit_Point = 100;
            this.Melee_Power = GetRandomNumberWithinRange(1, 3);
            this.Magic_Power = GetRandomNumberWithinRange(3, 8);
            this.Defense_Power = 0;
            Position = new Position();
        }

        public void AddStaffToWarrior(Staff staff)
        {
            StaffForWarrior = staff;
            StaffForWarrior.Position = this.Position;

            if (StaffForWarrior != null && StaffForWarrior.Available == true)
            {
                this.Magic_Power = this.Magic_Power + StaffForWarrior.StaffPower;
                StaffForWarrior.Available = false;
            }
        }

        public void DropStaff()
        {
            StaffForWarrior.Position = this.Position;
            StaffForWarrior.Available = true;
        }

        public void SetStaffPosition(Position pos)
        {
            StaffForWarrior.Position = pos;
        }
    }
}
