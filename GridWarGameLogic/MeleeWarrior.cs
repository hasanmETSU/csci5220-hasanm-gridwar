﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GridWarGameLogic
{
    public class MeleeWarrior : Warrior
    {

        //public String MeleeWarriorName = "MeleeWarrior";
        public static int MeleeWarriorNumber = 0;
       // double MeleePower, MagicPower;

        public Sword SwordForWarrior;

        public MeleeWarrior() 
        {
            
            this.Hit_Point = 100;
            this.Melee_Power = GetRandomNumberWithinRange(5, 10);
            this.Magic_Power = GetRandomNumberWithinRange(1, 3);
            this.Defense_Power = 0;
            Position = new Position();

        }

        public void AddSwordToWarrior(Sword sword)
        {
            SwordForWarrior = sword;
            SwordForWarrior.Position = this.Position;
            if(SwordForWarrior != null && SwordForWarrior.Available == true)
            {
                this.Melee_Power = this.Melee_Power + SwordForWarrior.SwordPower;
                SwordForWarrior.Available = false;
            }
        }

        public void DropSword()
        {
            SwordForWarrior.Position = this.Position;
            SwordForWarrior.Available = true;
        }

        public void SetSwordPosition(Position pos)
        {
            SwordForWarrior.Position = pos;
        }
    }
}
