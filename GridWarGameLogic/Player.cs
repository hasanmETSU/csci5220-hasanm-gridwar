﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GridWarGameLogic
{
    public class Player
    {
        public String Name { set; get; }
        public int NumberOfMeleeWarrior { set; get; }
        public int NumberOfMagicWarrior { set; get; }
        public int TotalNumberOfWarrior { set; get; }
        public String MeleeWarriorName = "MeleeWarrior";
        public String MagicWarriorName = "MagicWarrior";
        public Dictionary<String, Warrior> Warriors;
        public int RowLimitFirst { set; get; }
        public int RowLimitSecond { set; get; }
       
        public Player(String name)
        {
            this.Name = name;
            Warriors = new Dictionary< String, Warrior > ();
            //PlayersWarriors = new Dictionary<String, List<Warrior>>();
        }

        // Select the warrior type
        public void SelectWarrior(int MeleeWarriors, int MagicWarriors)
        {

            NumberOfMeleeWarrior = MeleeWarriors;
            NumberOfMagicWarrior = MagicWarriors;

            TotalNumberOfWarrior = NumberOfMeleeWarrior + NumberOfMagicWarrior;

        }

       
        public void CreateArmy()
        {
            // create Melee Warrior 
            for (int i = 1; i <= NumberOfMeleeWarrior; i++)
            {
                Warriors.Add(MeleeWarriorName+i, new MeleeWarrior());
            }

            // create Magic Warrior 
            for (int i = 1; i <= NumberOfMagicWarrior; i++)
            {

                Warriors.Add(MagicWarriorName+i, new MagicWarrior());
            }
        }


        public void SetPlayerPositionInBoard(String position)
        {
            if(position.CompareTo("top") == 0)
            {
                RowLimitFirst = 0;
                RowLimitSecond = 1;
            }

            if( position.CompareTo("bottom") == 0 )
            {
                RowLimitFirst = 4;
                RowLimitSecond = 5;
            }
        }


        public void PlaceWarriorInGridBoard(String warrior, int row, int col, GridBoard gridBoard)
        {
            Warrior warriorObj = Warriors[warrior];
            if ((row >= RowLimitFirst && row <= RowLimitSecond) && gridBoard.isValidPosition(row, col) && gridBoard.isPositionAvailable(row, col))
            {
                warriorObj.Position.setPosition(row, col);
                gridBoard.InsertValueIntoBoard(row, col, 1);
                
            }
        }

        public void addWeaponToWarrior( String warrior)
        {
            Warrior warriorObj = Warriors[warrior];

            if (warrior.Contains("Melee"))
            {
                MeleeWarrior melee = (MeleeWarrior)warriorObj;
                melee.AddSwordToWarrior(new Sword());
            }
            else
            {
                MagicWarrior magic = (MagicWarrior)warriorObj;
                magic.AddStaffToWarrior(new Staff());
            }
                

        }


        public void Attack(String warriorName, String AttackType, Position position, Player OpponentPlayer)
        {
            Double MeleeDamage, MagicDamage;
            Warrior warrior = Warriors[warriorName];
            Warrior Opponent = null, opponentToAttack = null;
            //Find the opponent
            foreach (KeyValuePair<String, Warrior>  pair in OpponentPlayer.Warriors)
            {
                Opponent = pair.Value;
                if((Opponent.Position.Row == position.Row) && (Opponent.Position.Col == position.Col))
                {
                    opponentToAttack = Opponent;
                }
            }
            
           

            if (AttackType.ToLower().CompareTo("melee") == 0)
            {
                MeleeDamage = opponentToAttack.Melee_Power - (opponentToAttack.Defense_Power/100 * opponentToAttack.Melee_Power);
                opponentToAttack.Hit_Point = opponentToAttack.Hit_Point - Convert.ToInt32(MeleeDamage); 
            }

            if(AttackType.ToLower().CompareTo("magic") == 0)
            {
                MagicDamage = opponentToAttack.Magic_Power - (opponentToAttack.Defense_Power/100 * opponentToAttack.Magic_Power);
                opponentToAttack.Hit_Point = opponentToAttack.Hit_Point - Convert.ToInt32(MagicDamage);
            }

            // if Vanquish an opponent
            if(opponentToAttack.Hit_Point <= 0 )
            {
                warrior.Defense_Power += 1;

                if(warrior.Name.ToLower().Contains("melee") )
                {
                    warrior.Melee_Power += 1;
                    warrior.Magic_Power += 0.25;
                }

                if (warrior.Name.ToLower().Contains("magic"))
                {
                    warrior.Melee_Power += 0.25;
                    warrior.Magic_Power += 1;
                }
            }

            // if Attack an Opponent
            if (warrior.Name.ToLower().Contains("melee"))
            {
                warrior.Melee_Power += 0.5;
                warrior.Magic_Power += 0.125;
            }

            if (warrior.Name.ToLower().Contains("magic"))
            {
                warrior.Melee_Power += 0.125;
                warrior.Magic_Power += 0.5;
            }

            // increase opponent defense power
            opponentToAttack.Defense_Power += 0.25;

        }

        // To move a warrior
        public void Move(String warriorName, GridBoard gridboard, String MoveDirection)
        {
            int MoveOrAttackRow = -1, MoveOrAttackCol = -1;
             Warrior warrior = Warriors[warriorName];

            if (MoveDirection.CompareTo("North") == 0)
            {
                MoveOrAttackRow = warrior.Position.Row - 1;
                MoveOrAttackCol = warrior.Position.Col;
            }
            else if (MoveDirection.CompareTo("South") == 0)
            {
                MoveOrAttackRow = warrior.Position.Row + 1;
                MoveOrAttackCol = warrior.Position.Col;
            }
            else if (MoveDirection.CompareTo("East") == 0)
            {
                MoveOrAttackRow = warrior.Position.Row;
                MoveOrAttackCol = warrior.Position.Col - 1;
            }
            else if (MoveDirection.CompareTo("West") == 0)
            {
                MoveOrAttackRow = warrior.Position.Row;
                MoveOrAttackCol = warrior.Position.Col + 1;
            }
            else if (MoveDirection.CompareTo("North East") == 0)
            {
                MoveOrAttackRow = warrior.Position.Row - 1;
                MoveOrAttackCol = warrior.Position.Col - 1;
            }
            else if (MoveDirection.CompareTo("North West") == 0)
            {
                MoveOrAttackRow = warrior.Position.Row - 1;
                MoveOrAttackCol = warrior.Position.Col + 1;
            }
            else if (MoveDirection.CompareTo("South West") == 0)
            {
                MoveOrAttackRow = warrior.Position.Row + 1;
                MoveOrAttackCol = warrior.Position.Col + 1;
            }
            else if (MoveDirection.CompareTo("South East") == 0)
            {
                MoveOrAttackRow = warrior.Position.Row + 1;
                MoveOrAttackCol = warrior.Position.Col - 1;
            }

            if (gridboard.isValidPosition(MoveOrAttackRow, MoveOrAttackCol) && gridboard.isPositionAvailable(MoveOrAttackRow, MoveOrAttackCol))
            {
                warrior.Position.Row = MoveOrAttackRow;
                warrior.Position.Col = MoveOrAttackCol;
                gridboard.InsertValueIntoBoard(MoveOrAttackRow, MoveOrAttackCol, 1);
            }
        }

        public void ShowStatistics(String warrior)
        {
            Console.WriteLine("ShowStatistics  Action");
        }

        public void Surrender()
        {
            Console.WriteLine("Surrender Action");
        }

    }
}
