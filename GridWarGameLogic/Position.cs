﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GridWarGameLogic
{
    public class Position
    {
        public int Row { set; get; }
        public int Col { set; get; }


        public Position()
        {
            Row = -1;
            Col = -1;
        }

        public void setPosition(int row, int col)
        {
            Row = row;
            Col = col;
        }

        
    }
}
