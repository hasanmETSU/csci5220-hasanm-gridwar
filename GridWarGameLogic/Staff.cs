﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GridWarGameLogic
{

    public class Staff
    {
        public Double StaffPower { get; set; }
        public Boolean Available { get; set; }
        public Position Position { get; set; }

        public Staff()
        {
            Random random = new Random();
            int power = random.Next(2, 4);
            StaffPower = Convert.ToDouble(power);
            Available = true;
        }

       
    }
}
