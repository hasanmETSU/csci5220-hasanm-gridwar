﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GridWarGameLogic
{
    public class Sword 
    {
        public Double SwordPower { get; set; }
        public Boolean Available { get; set; }
        public Position Position { get; set; }

        public Sword()
        {
            Random random = new Random();
            int power = random.Next(3, 6);
            SwordPower = Convert.ToDouble(power);
            Available = true;
        }
        
       
    }
}
