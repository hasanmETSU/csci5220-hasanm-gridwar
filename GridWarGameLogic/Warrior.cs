﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GridWarGameLogic
{
    public abstract class Warrior
    {
        public int Hit_Point { set; get; }
        public Double Melee_Power { set; get; }
        public Double Magic_Power { set; get; }
        public Double Defense_Power { set; get; }
        public String Name { set; get; }
        public Position Position { set; get; }

        public Warrior()
        {
            
        }

        // generate Randdom number within a range
        public Double GetRandomNumberWithinRange(int num1, int num2)
        {
            Random random = new Random();
            int randomNumber = random.Next(num1, num2);
            return Convert.ToDouble(randomNumber);
        }

        

    }
}
