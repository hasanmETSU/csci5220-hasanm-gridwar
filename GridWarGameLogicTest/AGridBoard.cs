﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using GridWarGameLogic;


namespace GridWarGameLogicTest
{
    [TestFixture]
    public class AGridBoard
    {
        [Test]
        public void ShouldHaveSixBySixSizeInitializedWhenConstructed()
        {
            var sut = new GridBoard();
            Assert.That(sut.Row, Is.EqualTo(6));
            Assert.That(sut.Col, Is.EqualTo(6));
        }

        [Test]
        public void ShouldHaveCreateSixBySixBoard()
        {
            var sut = new GridBoard();
            Assert.That(sut.board.GetLength(0), Is.EqualTo(6));
            Assert.That(sut.board.GetLength(1), Is.EqualTo(6));
        }

        [Test]
        public void ShouldHaveVerifiedAPositionIsValid()
        {
            var sut = new GridBoard();
            Assert.That(sut.isValidPosition(4, 5), Is.True);
            
        }

        [Test]
        public void ShouldHaveVerifiedAPositionIsAvailable()
        {
            var sut = new GridBoard();
            //sut.createGridBoard();
            Assert.That(sut.isPositionAvailable(4, 5), Is.True);

        }


        [Test]
        public void ShouldHaveInsertValueAtSpecifiedPositon()
        {
            var sut = new GridBoard();
            //sut.createGridBoard();
            sut.InsertValueIntoBoard(1, 2, 1);
            Assert.That(sut.board[1,2], Is.EqualTo(1));

        }

    }
}
