﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using GridWarGameLogic;


namespace GridWarGameLogicTest
{
    [TestFixture]
    public class AMagicWarrior
    {
        [Test]
        public void ShouldHavaeMileePowerOneToThreeRangeWhenConstructed()
        {
            var sut = new MagicWarrior();
            Assert.That(sut.Melee_Power, Is.GreaterThanOrEqualTo(1));
            Assert.That(sut.Melee_Power, Is.LessThanOrEqualTo(3));
        }

        [Test]
        public void ShouldHaveMagicPowerThreeToEightRangeWhenConstructed()
        {
            var sut = new MagicWarrior();
            Assert.That(sut.Magic_Power, Is.GreaterThanOrEqualTo(3));
            Assert.That(sut.Magic_Power, Is.LessThanOrEqualTo(8));
        }

        [Test]
        public void ShouldHaveOneHundredHitPointWhenConstructed()
        {
            var sut = new MagicWarrior();
            Assert.That(sut.Hit_Point, Is.EqualTo(100));
        }

        [Test]
        public void ShouldHaveZeroDefensePowerWhenConstructed()
        {
            var sut = new MagicWarrior();
            Assert.That(sut.Defense_Power, Is.EqualTo(0));
        }

        [Test]
        public void ShouldHaveNoPositionWhenConstructed()
        {
            var sut = new MagicWarrior();
            Assert.That(sut.Position.Row, Is.EqualTo(-1));
            Assert.That(sut.Position.Col, Is.EqualTo(-1));
        }

        [Test]
        public void ShouldHaveUpdateStaffPositonAfterPositionSet()
        {
            var sut = new MagicWarrior();
            var staff = new Staff();
            sut.AddStaffToWarrior(staff);
            Assert.That(sut.Position.Row, Is.EqualTo(sut.StaffForWarrior.Position.Row));
            Assert.That(sut.Position.Col, Is.EqualTo(sut.StaffForWarrior.Position.Col));
        }

        [Test]
        public void ShouldHaveIncreaseMagicPowerAfterStaffAddedToWarrior()
        {
            var sut = new MagicWarrior();
            var MagicPowerAtPre = sut.Magic_Power;
            var staff = new Staff();
            sut.AddStaffToWarrior(staff);
            Assert.That(sut.Magic_Power, Is.EqualTo(MagicPowerAtPre + staff.StaffPower));
        }

        [Test]
        public void ShouldHaveStaffAvailabityFalseAfterStaffAddedToWarrior()
        {
            var sut = new MagicWarrior();
            var staff = new Staff();
            sut.AddStaffToWarrior(staff);
            Assert.That(sut.StaffForWarrior.Available, Is.False);
        }

        [Test]
        public void ShouldHaveSwordAvailabityTrueAfterStaffDroppedFromWarrior()
        {
            var sut = new MagicWarrior();
            var staff = new Staff();
            sut.AddStaffToWarrior(staff);
            sut.DropStaff();
            Assert.That(sut.StaffForWarrior.Available, Is.True);
        }
    }
}
