﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using GridWarGameLogic;

namespace GridWarGameLogicTest
{
    [TestFixture]
    public class AMileeWarrior
    {
        [Test]
        public void ShouldHavaeMileePowerFiveToTenRangeWhenConstructed()
        {
            var sut = new MeleeWarrior();
            Assert.That(sut.Melee_Power, Is.GreaterThanOrEqualTo(5));
            Assert.That(sut.Melee_Power, Is.LessThanOrEqualTo(10));
        }

        [Test]
        public void ShouldHavaeMagicPowerOneToThreeRangeWhenConstructed()
        {
            var sut = new MeleeWarrior();
            Assert.That(sut.Magic_Power, Is.GreaterThanOrEqualTo(1));
            Assert.That(sut.Magic_Power, Is.LessThanOrEqualTo(3));
        }

        [Test]
        public void ShouldHaveOneHundredHitPointWhenConstructed()
        {
            var sut = new MeleeWarrior();
            Assert.That(sut.Hit_Point, Is.EqualTo(100));       
        }

        [Test]
        public void ShouldHaveZeroDefensePowerWhenConstructed()
        {
            var sut = new MeleeWarrior();
            Assert.That(sut.Defense_Power, Is.EqualTo(0));
        }

        [Test]
        public void ShouldHaveNoPositionWhenConstructed()
        {
            var sut = new MeleeWarrior();
            Assert.That(sut.Position.Row, Is.EqualTo(-1));
            Assert.That(sut.Position.Col, Is.EqualTo(-1));
        }

        [Test]
        public void ShouldHaveUpdateSwordPositonAfterAssignToWarrior()
        {
            var sut = new MeleeWarrior();
            var sword = new Sword();
            sut.AddSwordToWarrior(sword);
            Assert.That(sut.Position.Row, Is.EqualTo(sut.SwordForWarrior.Position.Row));
            Assert.That(sut.Position.Col, Is.EqualTo(sut.SwordForWarrior.Position.Col));
        }

        [Test]
        public void ShouldHaveIncreaseMeleePowerAfterSwordAddedToWarrior()
        {
            var sut = new MeleeWarrior();
            var MeleePowerAtPre = sut.Melee_Power;
            var sword = new Sword();
            sut.AddSwordToWarrior(sword);
            Assert.That(sut.Melee_Power, Is.EqualTo(MeleePowerAtPre + sword.SwordPower));
        }

        [Test]
        public void ShouldHaveSwordAvailabityFalseAfterSwordAddedToWarrior()
        {
            var sut = new MeleeWarrior();
            var sword = new Sword();
            sut.AddSwordToWarrior(sword);
            Assert.That(sut.SwordForWarrior.Available, Is.False);
        }

        [Test]
        public void ShouldHaveSwordAvailabityTrueAfterSwordDroppedFromWarrior()
        {
            var sut = new MeleeWarrior();
            var sword = new Sword();
            sut.AddSwordToWarrior(sword);
            sut.DropSword();
            Assert.That(sut.SwordForWarrior.Available, Is.True);
        }

        [Test]
        public void ShouldHaveUpdatePositionAfterSwordDroppedFromWarrior()
        {
            var sut = new MeleeWarrior();
            var sword = new Sword();
            sut.AddSwordToWarrior(sword);
            sut.DropSword();
            Assert.That(sut.SwordForWarrior.Position.Row, Is.EqualTo(sut.Position.Row));
            Assert.That(sut.SwordForWarrior.Position.Col, Is.EqualTo(sut.Position.Col));
        }
    }
}
