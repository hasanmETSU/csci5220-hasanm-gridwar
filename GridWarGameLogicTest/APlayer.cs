﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using GridWarGameLogic;


namespace GridWarGameLogicTest
{
    [TestFixture]
    public class APlayer
    {
       [Test]
       public void ShouldHaveTotalSixNumberOfWarrior()
        {
            var sut1 = new Player("player1");
            var sut2 = new Player("player2");

            sut1.SelectWarrior(3, 3);
            sut2.SelectWarrior(3, 3);

            sut1.CreateArmy();
            sut2.CreateArmy();
            Assert.That(sut1.TotalNumberOfWarrior, Is.EqualTo(sut1.Warriors.Count));
            Assert.That(sut2.TotalNumberOfWarrior, Is.EqualTo(sut2.Warriors.Count));
        }

        [Test]
        public void ShouldHaveZeroOrOneRowForTopAndFourOrFiveForOppositePosition()
        {
            var sut1 = new Player("player1");
            var sut2 = new Player("player2");
            sut1.SetPlayerPositionInBoard("top");
            sut2.SetPlayerPositionInBoard("bottom");
            Assert.That(sut1.RowLimitFirst, Is.EqualTo(0));
            Assert.That(sut1.RowLimitSecond, Is.EqualTo(1));
            Assert.That(sut2.RowLimitFirst, Is.EqualTo(4));
            Assert.That(sut2.RowLimitSecond, Is.EqualTo(5));
        }

        [Test]
        public void ShouldHaveSetTheWarriorInGridBoard()
        {
            var gridBoard = new GridBoard();
            var sut1 = new Player("player1");
            var sut2 = new Player("player2");

            sut1.SelectWarrior(3, 3);
            sut2.SelectWarrior(3, 3);

            sut1.CreateArmy();
            sut2.CreateArmy();
            sut1.SetPlayerPositionInBoard("top");
            sut2.SetPlayerPositionInBoard("bottom");
            sut1.PlaceWarriorInGridBoard("MeleeWarrior1", 0, 1, gridBoard);
            sut2.PlaceWarriorInGridBoard("MeleeWarrior2", 4, 4, gridBoard);
            Assert.That(sut1.Warriors["MeleeWarrior1"].Position.Row, Is.EqualTo(0));
            Assert.That(sut1.Warriors["MeleeWarrior1"].Position.Col, Is.EqualTo(1));
            Assert.That(gridBoard.board[0,1], Is.EqualTo(1));

            Assert.That(sut2.Warriors["MeleeWarrior2"].Position.Row, Is.EqualTo(4));
            Assert.That(sut2.Warriors["MeleeWarrior2"].Position.Col, Is.EqualTo(4));
            Assert.That(gridBoard.board[4, 4], Is.EqualTo(1));
        }

        [Test]
        public void ShouldBeAbleToMoveWarriorInGridBoard()
        {
            var gridBoard = new GridBoard();
            var sut1 = new Player("player1");
            var sut2 = new Player("player2");

            sut1.SelectWarrior(3, 3);
            sut2.SelectWarrior(3, 3);

            sut1.CreateArmy();
            sut2.CreateArmy();
            sut1.SetPlayerPositionInBoard("top");
            sut2.SetPlayerPositionInBoard("bottom");
            sut1.PlaceWarriorInGridBoard("MeleeWarrior1", 0, 1, gridBoard);
            sut2.PlaceWarriorInGridBoard("MeleeWarrior2", 4, 4, gridBoard);
            sut1.Move("MeleeWarrior1", gridBoard, "South West");
            sut2.Move("MeleeWarrior2", gridBoard, "North");

            Assert.That(sut1.Warriors["MeleeWarrior1"].Position.Row, Is.EqualTo(1));
            Assert.That(sut1.Warriors["MeleeWarrior1"].Position.Col, Is.EqualTo(2));
            Assert.That(gridBoard.board[0, 1], Is.EqualTo(1));

            Assert.That(sut2.Warriors["MeleeWarrior2"].Position.Row, Is.EqualTo(3));
            Assert.That(sut2.Warriors["MeleeWarrior2"].Position.Col, Is.EqualTo(4));
            Assert.That(gridBoard.board[4, 4], Is.EqualTo(1));
        }

        [Test]
        public void ShouldbeAbleToAttackAnotherWarrior()
        {
            var gridBoard = new GridBoard();
            var sut1 = new Player("player1");
            var sut2 = new Player("player2");
            var position = new Position();
            sut1.SelectWarrior(3, 3);
            sut2.SelectWarrior(3, 3);

            sut1.CreateArmy();
            sut2.CreateArmy();
            sut1.SetPlayerPositionInBoard("top");
            sut2.SetPlayerPositionInBoard("bottom");
            sut1.PlaceWarriorInGridBoard("MagicWarrior1", 0, 1, gridBoard);
            sut2.PlaceWarriorInGridBoard("MagicWarrior2", 4, 4, gridBoard);
            sut1.Move("MagicWarrior1", gridBoard, "South West");
            sut2.Move("MagicWarrior2", gridBoard, "South West");
            position.Row = 1;
            position.Col = 2;

            var MagicPowerAtPre = sut2.Warriors["MagicWarrior2"].Magic_Power;
            var MeleePowerAtPre = sut2.Warriors["MagicWarrior2"].Melee_Power;


            sut2.Attack("MagicWarrior2", "magic", position, sut1);

           
            Assert.That(sut2.Warriors["MagicWarrior2"].Melee_Power, Is.EqualTo(MeleePowerAtPre+0.125));
            Assert.That(sut2.Warriors["MagicWarrior2"].Magic_Power, Is.EqualTo(MagicPowerAtPre+0.5));
            
        }
    }
}
