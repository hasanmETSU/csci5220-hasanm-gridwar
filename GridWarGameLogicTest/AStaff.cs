﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using GridWarGameLogic;

namespace GridWarGameLogicTest
{
    [TestFixture]
    public class AStaff
    {
        [Test]
        public void ShouldHaveStaffPowerTwoToFourRangeWhenConstructed()
        {
            var sut = new Staff();
            Assert.That(sut.StaffPower, Is.GreaterThanOrEqualTo(2));
            Assert.That(sut.StaffPower, Is.LessThanOrEqualTo(4));
        }

        [Test]
        public void ShouldHaveStaffAvailabilityTrueWhenConstructed()
        {
            var sut = new Staff();
            Assert.That(sut.Available, Is.True);
        }
    }
}
