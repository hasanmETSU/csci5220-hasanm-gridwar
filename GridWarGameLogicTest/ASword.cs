﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using GridWarGameLogic;


namespace GridWarGameLogicTest
{
    [TestFixture]
    public class ASword
    {
        [Test]
        public void ShouldHaveSwordPowerThreeToSixRangeWhenConstructed()
        {
            var sut = new Sword();
            Assert.That(sut.SwordPower, Is.GreaterThanOrEqualTo(3));
            Assert.That(sut.SwordPower, Is.LessThanOrEqualTo(6));
        }

        [Test]
        public void ShouldHaveSwordAvailabilityTrueWhenConstructed()
        {
            var sut = new Sword();
            Assert.That(sut.Available, Is.True);
        }
    }
}
